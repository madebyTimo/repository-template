#!/usr/bin/env bash
set -e

APPLICATION_NAME="###APPLICATION_NAME###"
DEPENDENCIES=(docker)
REPOSITORY_NAME="###REPOSITORY_NAME###"
SCRIPT_DIR="$(dirname "$(realpath "$0")")"

# help message
for ARGUMENT in "$@"; do
    if [ "$ARGUMENT" == "-h" ] || [ "$ARGUMENT" == "--help" ]; then
        echo "usage: $(basename "$0")"
        echo "Build npm project in docker."
        exit
    fi
done

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

# check arguments
while [[ -n "$1" ]]; do
    if [[ "$1" = "--update-base" ]]; then
        exit
    fi
    shift
done

PROJECT_DIR="$(dirname "$SCRIPT_DIR")"
VERSION="$(cat Version.txt)"

cd "$PROJECT_DIR"
mkdir --parents builds

docker run --pull always --rm \
    --volume "${REPOSITORY_NAME}-temp:/media/workdir/node_modules" \
    --volume "${PROJECT_DIR}:/media/workdir" \
    --workdir /media/workdir \
    madebytimo/nodejs \
    bash -c "set -e
    rm -rf .temp-build-web
    npm clean-install
    npm run build
    "

docker volume rm "${REPOSITORY_NAME}-temp"


if [[ -d .temp-build-web ]]; then
    docker run --pull always --rm \
        --volume "${PROJECT_DIR}:/media/workdir" \
        --workdir /media/workdir \
        madebytimo/scripts \
        bash -c "set -e
        rm -f builds/${APPLICATION_NAME}-${VERSION}.tar.zst
        compress.sh --output builds/${APPLICATION_NAME}-${VERSION}.tar.zst .temp-build-web/*
        rm -r .temp-build-web
        "
fi
