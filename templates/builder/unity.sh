#!/usr/bin/env bash
set -e

DEPENDENCIES=(docker)
REPOSITORY_NAME="###REPOSITORY_NAME###"
SCRIPT_DIR="$(dirname "$(realpath "$0")")"
UNITY_MODULE="windows-mono-3"

# help message
for ARGUMENT in "$@"; do
    if [ "$ARGUMENT" == "-h" ] || [ "$ARGUMENT" == "--help" ]; then
        echo "usage: $(basename "$0")"
        echo "Build unity project in docker."
        exit
    fi
done

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

# check arguments
while [[ -n "$1" ]]; do
    if [[ "$1" = "--update-base" ]]; then
        exit
    fi
    shift
done

PROJECT_DIR="$(dirname "$SCRIPT_DIR")"
VERSION="$(cat Version.txt)"

UNITY_VERSION="$(grep "m_EditorVersion: " ProjectSettings/ProjectVersion.txt \
    | sed "s|m_EditorVersion: ||")"

if [[ -z "$UNITY_VERSION" ]]; then
    echo "Unity version unknown"
    exit 1
fi

echo "Unity version \"${UNITY_VERSION}\" detected"


if [[ ! -f ${PROJECT_DIR}/data-local/unity-license.ulf ]]; then
    mkdir --parents builds
    touch "builds/unity-license-activation.alf"
    docker run --pull always --rm \
        --volume "${PROJECT_DIR}/builds/unity-license-activation.alf:/Unity_v${UNITY_VERSION}.alf" \
        "unityci/editor:${UNITY_VERSION}-${UNITY_MODULE}" \
        unity-editor \
            -batchmode \
            -createManualActivationFile \
            -logfile /dev/stdout
    echo "Please get licese file with \"builds/unity-license-activation.ald\"."
    echo "Instructions are available at https://docs.unity3d.com/Manual/ManualActivationCmdMac.html"
    echo "Please place the license file in data-local/unity-license.ulf"
else
    docker run --pull always --rm \
        --volume "${PROJECT_DIR}/data-local/unity-license.ulf:/unity-license.ulf" \
        --volume "${PROJECT_DIR}:/media/workdir" \
        --volume "${REPOSITORY_NAME}-build-cache:/media/workdir/Library" \
        "unityci/editor:${UNITY_VERSION}-${UNITY_MODULE}" \
        bash -c \
        "unity-editor \
            -batchmode \
            -logfile /dev/stdout \
            -manualLicenseFile /unity-license.ulf \
            -quit \
        && unity-editor \
            -batchmode \
            -buildVersion $VERSION \
            -executeMethod Builder.Build \
            -logfile /dev/stdout \
            -projectPath /media/workdir \
            -quit"
fi
