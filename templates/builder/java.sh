#!/usr/bin/env bash
set -e

DEPENDENCIES=(docker)
REPOSITORY_NAME="###REPOSITORY_NAME###"
SCRIPT_DIR="$(dirname "$(realpath "$0")")"

# help message
for ARGUMENT in "$@"; do
    if [ "$ARGUMENT" == "-h" ] || [ "$ARGUMENT" == "--help" ]; then
        echo "usage: $(basename "$0")"
        echo "Build java project in docker."
        exit
    fi
done

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

PROJECT_DIR="$(dirname "$SCRIPT_DIR")"
VERSION="$(cat Version.txt)"

mkdir --parents builds

docker run --pull always --rm \
    --env "VERSION=$VERSION" \
    --volume "${REPOSITORY_NAME}-build-cache:/root/.m2" \
    --volume "${PROJECT_DIR}:/media/workdir" \
    --workdir /media/workdir \
    madebytimo/java-maven \
    mvn package

echo "#!/usr/bin/env -S java -jar" > "builds/${REPOSITORY_NAME,,}"
cat "builds/${REPOSITORY_NAME}-${VERSION}.jar" >> "builds/${REPOSITORY_NAME,,}"
chmod +x "builds/${REPOSITORY_NAME,,}"
