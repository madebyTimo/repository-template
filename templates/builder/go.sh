#!/usr/bin/env bash
set -e

DEPENDENCIES=(docker)
OPERATING_SYSTEMS=(darwin linux windows)
PLATFORMS=(amd64 arm64)
REPOSITORY_NAME="###REPOSITORY_NAME###"
SCRIPT_DIR="$(dirname "$(realpath "$0")")"

# help message
for ARGUMENT in "$@"; do
    if [ "$ARGUMENT" == "-h" ] || [ "$ARGUMENT" == "--help" ]; then
        echo "usage: $(basename "$0")"
        echo "Build go project in docker."
        echo "--os OS Build only for specified operating system."
        echo "--platform [amd64|arm64|arm] Build only for specified platform."
        exit
    fi
done

# check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

# check arguments
while [[ -n "$1" ]]; do
    if [[ "$1" = "--platform" ]]; then
        shift
        PLATFORMS=("$1")
    elif [[ "$1" = "--update-base" ]]; then
        exit
    elif [[ "$1" = "--os" ]]; then
        shift
        OPERATING_SYSTEMS=("$1")
    fi
    shift
done

PROJECT_DIR="$(dirname "$SCRIPT_DIR")"
VERSION="$(cat Version.txt)"

cd "$PROJECT_DIR"
mkdir --parents builds

docker run --pull always --rm \
    --env "GOCACHE=/media/build-cache/gocache" \
    --env "GOPATH=/media/build-cache/gopath" \
    --env "VERSION=$VERSION" \
    --volume "${REPOSITORY_NAME}-build-cache:/media/build-cache" \
    --volume "${PROJECT_DIR}:/media/workdir" \
    --workdir /media/workdir \
    madebytimo/go \
    bash -c "set -e
    for GOARCH in ${PLATFORMS[*]}; do
        for GOOS in ${OPERATING_SYSTEMS[*]}; do
            for COMMAND in ./cmd/*; do
                echo \"Building \${COMMAND#./cmd/} for \${GOOS}-\${GOARCH}\"
                OUTPUT=\"builds/\${COMMAND#./cmd/}-${VERSION}-\${GOOS}-\${GOARCH}\"
                if [[ \${GOOS} == windows ]]; then
                    OUTPUT+=.exe
                fi
                export GOARCH GOOS
                go build -o \"\${OUTPUT}\" \"\${COMMAND}\"
            done
        done
    done"
