package main

import (
	"flag"
	"log"
)

type CommandLineArguments struct {
	outputPath string
}

func main() {
	log.Default().SetFlags(0)
	commandLineArguments := parseCommandLineArguments()
	printConfiguration(commandLineArguments)
}

func parseCommandLineArguments() CommandLineArguments {
	outputPath := flag.String("output", ".", "The output directory.")
	flag.Parse()
	return CommandLineArguments{
		outputPath: *outputPath,
	}
}

func printConfiguration(commandLineArguments CommandLineArguments) {
	log.Println("Configuration:")
	log.Printf("Output Directory: %s", commandLineArguments.outputPath)
	log.Println()
}
