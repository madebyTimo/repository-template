#!/usr/bin/env bash
set -e -o pipefail

CALLER_DIR="${PWD}"
DEPENDENCIES=()
LOCK_FILE="/run/lock/$(basename "$0").lock"
PULL_POLICY="${PULL_POLICY:-always}"
SCRIPT_DIR="$(dirname "$(realpath "$0")")"
WORKING_DIR="${TMPDIR:-/tmp}/temp-$(basename "$0")-$(cat /proc/sys/kernel/random/uuid)"

# Help message
for ARGUMENT in "$@"; do
    if [ "$ARGUMENT" == "-h" ] || [ "$ARGUMENT" == "--help" ]; then
        echo "Usage: $(basename "$0") [ARGUMENT]"
        echo "Does a lot of magic."
        echo "ARGUMENT can be"
        echo "    --argument PARAMETER Do argument magic with the parameter, default: trick"
        exit
    fi
done

# Check if run as root
if [[ "$(id --user)" != 0 ]]; then
    echo "Script must run as root"
    if [[ -n "$(which sudo)" ]];then
        echo "Try with sudo"
        sudo "$0" "$@"
        exit
    fi
    exit 1
fi

# Check dependencies
for CMD in "${DEPENDENCIES[@]}"; do
    if [[ -z "$(which "$CMD")" ]]; then
        echo "\"${CMD}\" is missing!"
        exit 1
    fi
done

# Aquire lock
if [ -e "$LOCK_FILE" ]; then
    echo "Error: $(basename "$0") is already running. Exiting."
    exit 1
fi
touch "$LOCK_FILE"

function cleanup {
    rm -f -r "$WORKING_DIR"
    rm "$LOCK_FILE"
}
trap cleanup EXIT

# Check arguments
while [[ -n "$1" ]]; do
    if [[ "$1" == "--argument" ]]; then
        echo "magic"
    else
        echo "Unknown argument: \"$1\""
        exit 1
    fi
    shift
done

mkdir "$WORKING_DIR"
cd "$WORKING_DIR"
